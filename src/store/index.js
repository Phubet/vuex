import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './mutation-types'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    number: 0
  },
  mutations: {
    [types.PLUS] (state) {
      state.number++
    },
    [types.MINUS] (state) {
      state.number--
    }
  },
  actions: {
    plus ({ commit }) {
      commit(types.PLUS)
    },
    minus ({ commit, state }) {
      if (state.number === 0) return
      commit(types.MINUS)
    }
  },
  modules: {}
})
